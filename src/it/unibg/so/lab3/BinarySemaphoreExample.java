package it.unibg.so.lab3;

import java.util.concurrent.Semaphore;

public class BinarySemaphoreExample {
	
	private Semaphore semaphore = new Semaphore(1);
	
	public void sharedAccess(int id){
		try {
			semaphore.acquire();
			System.out.println("[thread " + id + "] acquired.");
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("[thread " + id + "] released.");
		semaphore.release();
	}

	public static void main(String[] args){
		BinarySemaphoreExample resource = new BinarySemaphoreExample();
		new SecondThread(resource).start();
		resource.sharedAccess(1);
	}
}

class SecondThread extends Thread{
	
	private BinarySemaphoreExample sharedresource;
	
	public SecondThread(BinarySemaphoreExample r) {
		sharedresource = r;
	}

	@Override
	public void run() {
		sharedresource.sharedAccess(2);
	}
}